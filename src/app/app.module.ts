import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes.component';
import { HeroDetailComponent } from './hero-detail.component';
import { DashboardComponent } from './dashboard.component';

import { DTMAnalyticsComponent } from './dtm-analytics.component';

import { AppRoutingModule } from './app-routing.module';

import { HeroService } from './hero.service';

@NgModule({
	imports: [
		BrowserModule,
		FormsModule, // <-- import the FormsModule before binding with [(ngModel)]
		AppRoutingModule
	],
	declarations: [
		AppComponent,
		DashboardComponent,
		HeroesComponent,
		HeroDetailComponent,
		DTMAnalyticsComponent
	],
	providers: [
		HeroService
	],
	bootstrap: [
		AppComponent,
		DTMAnalyticsComponent
	]
})


export class AppModule { }