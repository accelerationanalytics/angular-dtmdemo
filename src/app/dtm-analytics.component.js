"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
// import { Router, Event as NavigationEvent } from "@angular/router";
require("rxjs/add/operator/filter");
require("rxjs/add/operator/pairwise");
var dtmScriptTarget = 'https://assets.adobedtm.com/1d82bdb26f60b41afd8d68040d2bb7f39795f4ba/satelliteLib-fdae6dd6ce05e3c9cccd32de6469895359b89e3a-staging.js';
window.dataLayer = {
    pageName: "",
    pageCategory: "",
    pageSubSection1: "",
    pageSubSection2: "",
    websiteName: "Angular Test",
    websiteNameCode: "AG4"
};
var DTMAnalyticsComponent = (function () {
    function DTMAnalyticsComponent(router, location) {
        var _this = this;
        this.router = router;
        this.location = location;
        this.previousUrl = "";
        this.currentUrl = "";
        // router.events.forEach((event: NavigationEvent) => {
        // 	console.log(event, location.path());
        // });
        // router.events.subscribe((val) => {
        // 	console.log(val, this.location.path());
        // });
        router.events.filter(function (event) { return event instanceof router_1.NavigationEnd; })
            .pairwise()
            .subscribe(function (event) {
            // console.log(event, this.location.path(), this.location );
            // console.log(event, this.currentUrl, this.previousUrl, this.location.path());
            _this.gotoDetail(event);
            console.log('subscribe -----', event);
        });
    }
    ;
    DTMAnalyticsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var location = {
            url: this.location.path()
        };
        var dtmScript = document.createElement('script');
        dtmScript.src = dtmScriptTarget;
        dtmScript.type = 'text/javascript';
        dtmScript.onload = function () {
            console.log('DTM has been fully loaded');
            _satellite.pageBottom();
            _this.gotoDetail([location, location]);
        };
        dtmScript.async = true;
        dtmScript.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(dtmScript);
    };
    DTMAnalyticsComponent.prototype.analyticsGetPageDetails = function (currentUrl) {
        currentUrl = currentUrl.substr(1).replace(/\//g, ':').replace(/\-/, ' ');
        window.dataLayer.pageName = currentUrl;
        window.dataLayer.pageCategory = currentUrl.split(":")[0];
    };
    DTMAnalyticsComponent.prototype.gotoDetail = function (routeInfo) {
        // 
        this.previousUrl = routeInfo[0].url;
        this.currentUrl = routeInfo[1].url;
        // console.log(routeInfo, this.currentUrl, this.previousUrl, this.location.path());
        console.log('+++', routeInfo, this.previousUrl, this.currentUrl, '***', _satellite);
        this.analyticsGetPageDetails(this.currentUrl);
        if (_satellite !== undefined) {
            _satellite.track('globalVirtualPageView');
        }
    };
    return DTMAnalyticsComponent;
}());
DTMAnalyticsComponent = __decorate([
    core_1.Component({
        selector: 'dtm-analtics',
        template: "DTMloading..."
    }),
    __metadata("design:paramtypes", [router_1.Router,
        common_1.Location])
], DTMAnalyticsComponent);
exports.DTMAnalyticsComponent = DTMAnalyticsComponent;
//# sourceMappingURL=dtm-analytics.component.js.map