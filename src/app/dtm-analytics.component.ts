import { Component, OnInit }	from '@angular/core';
import { Location }	from '@angular/common';
import { Router, NavigationEnd } from "@angular/router";
// import { Router, Event as NavigationEvent } from "@angular/router";

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';

declare let window: any;
declare let _satellite: any;
declare let dataLayer: Object;

let dtmScriptTarget: string = 'https://assets.adobedtm.com/1d82bdb26f60b41afd8d68040d2bb7f39795f4ba/satelliteLib-fdae6dd6ce05e3c9cccd32de6469895359b89e3a-staging.js';
window.dataLayer = {
	pageName: "",
	pageCategory: "",
	pageSubSection1: "",
	pageSubSection2: "",
	websiteName: "Angular Test",
	websiteNameCode: "AG4"
}

@Component({
  selector: 'dtm-analtics',
  template: "DTMloading..."
})

export class DTMAnalyticsComponent implements OnInit {
	previousUrl: string = "";
	currentUrl: string = "";

	constructor(
		private router: Router,
		private location: Location
	) {
		// router.events.forEach((event: NavigationEvent) => {
		// 	console.log(event, location.path());
		// });
		// router.events.subscribe((val) => {
		// 	console.log(val, this.location.path());
		// });
		router.events.filter((event) => event instanceof NavigationEnd)
		.pairwise()
		.subscribe((event) => {
			// console.log(event, this.location.path(), this.location );
			// console.log(event, this.currentUrl, this.previousUrl, this.location.path());
			this.gotoDetail( event );
			console.log('subscribe -----', event);
		});

	};

	ngOnInit(): void {
		var location = {
			url: this.location.path()
		};

		let dtmScript = document.createElement('script');
		dtmScript.src = dtmScriptTarget;
		dtmScript.type = 'text/javascript';
		dtmScript.onload = () =>{
			console.log('DTM has been fully loaded');
			_satellite.pageBottom();
			this.gotoDetail( [ location, location] );
		};
		dtmScript.async = true;
		dtmScript.charset = 'utf-8';
		document.getElementsByTagName('head')[0].appendChild(dtmScript);
	}

	analyticsGetPageDetails(currentUrl: string): void {
		currentUrl = currentUrl.substr(1).replace(/\//g, ':').replace(/\-/, ' ');
		window.dataLayer.pageName = currentUrl;
		window.dataLayer.pageCategory = currentUrl.split(":")[0];
	}

	gotoDetail( routeInfo: Array<any> ): void {
		// 
		this.previousUrl = routeInfo[0].url;
		this.currentUrl = routeInfo[1].url;
		// console.log(routeInfo, this.currentUrl, this.previousUrl, this.location.path());
		console.log('+++', routeInfo, this.previousUrl, this.currentUrl , '***', _satellite);

		this.analyticsGetPageDetails(this.currentUrl);

		if(_satellite !== undefined) {
			_satellite.track('globalVirtualPageView');
		}
	}
	
}
